class CourseProgramsController < ApplicationController
	def show
	end
	def create
		@course = Course.find(params[:course_id]);
		@course_program = @course.course_programs.create(course_program_params)
		redirect_to controller: "courses", action:"manager_show", id:@course.id
	end
	def destroy
		@course = Course.find(params[:course_id])
		@course_program = @course.course_programs.find(params[:id])
		@course_program.destroy
		redirect_to controller: "courses", action:"manager_show", id:@course.id
	end
	

	private
	def course_program_params
		params.require(:course_program).permit(:lo, :contents, :time, :note)
	end
end
