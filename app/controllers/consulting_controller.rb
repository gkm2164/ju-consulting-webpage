class ConsultingController < ApplicationController
  before_action :call_consulting

  def call_consulting
    @consultings = Consulting.all
  end

  def index
  end

  def new
	@consulting = Consulting.new
  end
  def create
	@consulting = Consulting.new(consulting_params)
	if @consulting.save
		redirect_to controller: "consultings", action:"manager_index"
	else
		render 'new'
	end
  end
  def manager_index
	@consultings = Consulting.all
  end

  def show
    @consulting = Consulting.find(params[:id])
  end
  private
	def consulting_params
		params.require(:consulting).permit(:title, :banner_txt, :desc, :features, :main_img, :etc_img)
	end
end
