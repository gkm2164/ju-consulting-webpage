class CoursesController < ApplicationController
  def index
	@courses = Course.all
  end
  def manager_index
	@courses = Course.all
  end
  def edit
	@course = Course.find(params[:id])
  end
  def update
	@course = Course.find(params[:id])
	if @course.update(course_params)
		redirect_to @course
	else
		render 'edit'
	end
  end

  def show
	@courses = Course.all
	@course = Course.find(params[:id])
  end
  def manager_show
	@course = Course.find(params[:id])
  end
  def new
	@course = Course.new
  end
  def create
	@course = Course.new(course_params)
	if @course.save
		redirect_to controller: "courses", action:"manager_show", id: @course.id
	else
		render 'new'
	end
  end

  def destroy
	@course = Course.find(params[:id])
	@course.destroy

	redirect_to controller: "courses", action:"manager_index"
  end

  private
	def course_params
		params.require(:course).permit(:title, :purpose, :feature, :overview_number_of_memeber, :overview_type, :overview_target, :overview_time, :expectation)
	end
end
