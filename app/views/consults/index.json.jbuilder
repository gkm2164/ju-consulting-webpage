json.array!(@consults) do |consult|
  json.extract! consult, :id
  json.url consult_url(consult, format: :json)
end
