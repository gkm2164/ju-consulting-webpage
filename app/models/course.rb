class Course < ActiveRecord::Base
	has_many :course_programs, dependent: :destroy
end
