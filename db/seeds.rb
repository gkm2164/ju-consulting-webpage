# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)



#Course Data

Course.all.each { |y| y.delete }

course1 = Course.create(
                 title: "소프트웨어 요구사항 분석",
                 purpose: "이해관계자와의  효과적인 커뮤니케이션을 위한 모델링 방법 을 소개하여 소프트웨어 요구사항에 대한 공통의 이해를 추구하고 명확한 요구사항을 도출한다.",
                 feature: "실습을 통한 요구사항 명세, 검증, 관리 기법 습득\n구조화된 요구사항으로 요구사항 변경 추적\nCMM level  달성 또는 business 수행 방법 개선\nPM에 필수 : 프로젝트 비용 초과와 기간 지연 방지",
                 overview_number_of_member: 30,
                 overview_type: "집합교육(비합숙)",
                 overview_target: " 소프트웨어 과제 책임 관리자\n 소프트웨어 품질 보증 담당자\n소프트웨어  초/중/고급 개발자\n소프트웨어 시험 수행/관리자",
                 overview_time: "24h(3일)",
                 expectation: "이해관계자를 식별하고 이들에게서 수집한 needs를 효과적으로 분석하여 요구사항으로 기술할 수 있다. 또 이렇게 기술된 요구사항을 고객과 같이 평가하고 지속적으로 관리(변경, 위험)할 수 있다.", )
course1.course_programs.create(
                 lo: "이해관계자의 Needs 파악 및 요구사항 분석(LO-1, 2)",
                 contents: "요구공학 정의 및 프로세스 소개/요구사항 도출 소개/요구사항 모델링 방법 소개/요구사항 우선순위화 기법 소개/요구사항 선별/팀 단위의 요구사항 모델링 실습",
                 time: "8",
                 note: "실습 2h")
course1.course_programs.create(
                 lo: "요구사항 분석 및 기술(LO- 2,3)",
                 contents: "요구사항 작성 개요/품질 요구사항 정의/요구사항 작성 기법 소개/요구사항 기술 실습/Use Case 개요/use Case를 이용한 요구사항 모델링 기법 소개/ UML 모델 개발 실습",
                  time: "8",
                  note: "실습 3h")
course1.course_programs.create(
                  lo: "요구사항 평가 및 관리 (LO-4)",
                  contents: "요구사항 평가 활동 소개/요구사항 변경관리 프로세스/요구사항 추적성 정의 및 실습요구사항 개발/관리 프로세스/요구사항과 관련된 위험험 요소 추적/각 조별 실습 발표 및 과정 정리",
                  time: "8",
                  note: "발표 3h")
course2 = Course.create(
                 title: "소프트웨어 아키텍처 설계",
                 purpose: "S/W 아키텍처에 대한 개념을 이해하고 S/W 아키텍트가 되기 위한 조건이 무엇인지 알아본다. 또한 실전과 같은 예제를 기준으로 소프트웨어 아키텍처 수립하여 본다.",
                 feature: "S/W 아키텍처의 개념 정의\nS/W 아키텍트 역할 정의 및 필요한 Skill 정의\nS/W 아키텍처 뷰타입 개발 및 평가\n실전과 같은 S/W 아키텍처 수립 및 발표",
                 overview_number_of_member: "30",
                 overview_type: "집합교육(비합숙)",
                 overview_target: "중급 엔지니어(소프트웨어 설계/개발 경력 5년차 이상) 중 소프트웨어 아키텍트가 되고자 하시는 분",
                 overview_time: "24h(3일)",
                 expectation: "소프트웨어 아키텍처와 아키텍트의 개념을 알고 실전과 같은 소프트웨어 아키텍처를 수립함으로써 소프트웨어 아키텍트가 될 수 있는 기반을 마련한다.", )
 course2.course_programs.create(
                 lo: "소프트웨어 아키텍처 와 아키텍처 개념 (LO-1)",
                 contents: "소프트웨어 아키텍처 정의 및 목적/ 소프트웨어 아키텍트 역할 정의/ 소프트웨어 아키텍처 결정 요인/ 품질 요소 도출 실습",
                 time: "8",
                 note: "실습 2h")
 course2.course_programs.create(
                 lo: "소프트웨어 아키텍처 수립(LO-2,3,4,5)",
                 contents: "소프트웨어 아키텍처 뷰 정의/ 소프트웨어 아키텍처 뷰타입 소개 / 소프트웨어 아키텍팅 전략/ 참조 아키텍처 및 프레임웍/ 품질 요소별 아키텍팅 전략  / 소프트웨어 아키텍팅 실습",
                  time: "8",
                  note: "실습 3h")
 course2.course_programs.create(
                  lo: "소프트웨어 아키텍처 기술 및 평가 (LO-6,7)",
                  contents: "대안 아키텍처 평가 방법 소개/ 프로토타이핑 방법 소개소프트웨어 아키텍처 평가 실습/ 소프트웨어 아키텍팅 실습 결과 발표/ 과정 정리",
                  time: "8",
                  note: "실습 2h\n발표 2h")

course3 = Course.create(
                 title: "소프트웨어 아키텍처 개별 교육 - 패턴",
                 purpose: "S/W 아키텍처 뷰에 대해서 살펴보고 이를 작성하기 위한 아키텍처 전술 및  아키텍처 패턴에 대해서 살펴본다. 또한 실전에 사용되는 디자인 패턴과 비교를 한다. 실제 사용되는 아키텍처 패턴 및 참조 아키텍처를 소개하고 이에 대해 토론해본다.",
                 feature: "패턴 소개 및 실습 진행\nS/W 아키텍처 뷰별 아키텍처 전술 소개\nS/W 아키텍처 수립을 위한 아키텍처 전술, 패턴, 참조 아키텍처 소개\n디자인 패턴과 아키텍처 패턴의 연관관게 소개 및 디자인 패턴 개요 소개",
                 overview_number_of_member: "30",
                 overview_type: "집합교육(비합숙)",
                 overview_target: "고급 엔지니어(소프트웨어 설계/개발 경력 10차 정도) 혹은 소프트웨어 아키텍처 중급 강의를 들으신 분",
                 overview_time: "16h(8시간, 2일) or 16h(야간4시간, 4일)",
                 expectation: "보다 좋은 품질의 아키텍처를 수립할 수 있도록 소프트웨어 아키텍처에 다양한 패턴을 사용할 수 있도록 한다.", )
 course3.course_programs.create(
                 lo: "소프트웨어 전술 소개",
                 contents: "소프트웨어 아키텍처 정의 리뷰/소프트웨어 아키텍트 역할 리뷰/ 소프트웨어 아키텍처 수립에서 패턴의 역할 소개 / 소프트웨어 아키텍트 전술(tactics) 소개",
                 time: "4",
                 note: "")
 course3.course_programs.create(
                 lo: "소프트웨어 아키텍처 패턴 소개",
                 contents: "소프트웨어 아키텍팅 패턴 소개 (POSA 책 참고)/소프트웨어 아키텍팅 실습 ",
                  time: "4",
                  note: "실습 2h")
 course3.course_programs.create(
                  lo: "디자인 패턴 소개",
                  contents: "소프트웨어 아키텍처 패턴과 디자인 패턴(GoF 책 참고) 개요/ 디자인 패턴 개요 소개",
                  time: "4",
                  note: "")
course3.course_programs.create(
                  lo: "참조 아키텍처 소개 및 실전 사례 소개",
                  contents: "소프트웨어 아키텍처 참조 아키텍처 사례 소개/ 소프트웨어 아키텍처 패턴 적용 사례 소개 / 실제 개발 이슈 사례 소개 및 토론",
                  time: "8",
                  note: "실습 2h")
course4 = Course.create(
                 title: "소프트웨어 아키텍처 개별 교육 – 아키텍처 문서화",
                 purpose: "S/W 아키텍처를 수립하고 문서로 작성하는 방법에 대해서 소개한다. 아키텍처 뷰타입 라이브러리를 소개하고 이에 대한 실전 수립 사례를 살펴보고 토론해 본다.",
                 feature: "S/W 아키텍처 수립 및 문서 작성 프로세스 소개\nS/W 아키텍처 수립을 위한 아키텍처 뷰타입 라이브러리 소개\nS/W 아키텍처 뷰별 아키텍처 문서화 실습 및 토론 진행",
                 overview_number_of_member: "30",
                 overview_type: "집합교육(비합숙)",
                 overview_target: "고급 엔지니어(소프트웨어 설계/개발 경력 10차 정도) 혹은 소프트웨어 아키텍처 중급 강의를 들으신 분",
                 overview_time: "8h(8시간, 1일) or 8h(야간4시간, 2일)",
                 expectation: "기 수립된 소프트웨어 아키텍처를 문서로 작성할 수 있도록 한다.", )
course4.course_programs.create(
                 lo: "소프트웨어 아키텍처 수립 프로세스 소개",
                 contents: "소프트웨어 아키텍처 정의 리뷰/소프트웨어 아키텍처 수립 프로세스 소개 ",
                 time: "4",
                 note: "")
course4.course_programs.create(
                 lo: "소프트웨어 아키텍처 문서화 방법 소개",
                 contents: "소프트웨어 아키텍처 문서화 기술 방법 소개/소프트웨어 아키텍처 문서화 실습",
                  time: "4",
                  note: "실습 2h")
course5 = Course.create(
                 title: "소프트웨어 아키텍처 개별 교육 – 아키텍처 평가",
                 purpose: "S/W 아키텍처를 수립하면서 발생하는 다양한 대안 아키텍처 선택 방법에 대해서 알아본다. 또한 유명한 소프트웨어 아키텍처 평가 방법인 SAAM, ATAM, CBAM에 대해서 알아보고 실전에서 많이 쓰이는 프로토타입 기법에 대해서 알아본다. ",
                 feature: "S/W 아키텍처 수립시 발생하는 대안 아키텍처 선택 방법 소개\nS/W 아키텍처 평가 방법론인 SAAM, ATAM, CBAM  방법 소개\nS/W 아키텍처 평가로 현재 많이 쓰이는 프로토타입 방법에 대해 소개",
                 overview_number_of_member: "30",
                 overview_type: "집합교육(비합숙)",
                 overview_target: "고급 엔지니어(소프트웨어 설계/개발 경력 10차 정도) 혹은 소프트웨어 아키텍처 중급 강의를 들으신 분",
                 overview_time: "8h(8시간, 1일) or 8h(야간4시간, 2일)",
                 expectation: "소프트웨어 아키텍처 대안 발생 시 이를 효과적으로 해결할 수 있도록 하고 다양한 방법론에 이해하여 실전에 적용할 수 있도록 한다.", )
 course5.course_programs.create(
                 lo: "소프트웨어 대안 아키텍처 평가 방법 소개",
                 contents: "소프트웨어 아키텍처 정의 리뷰/소프트웨어 아키텍처 수립 프로세스 소개/ 소프트웨어 대안 평가 방법 소개 및 실전 사례 소개",
                 time: "2",
                 note: "")
 course5.course_programs.create(
                 lo: "소프트웨어 아키텍처 평가 방법론 소개",
                 contents: "SAAM, ATAM, CBAM 방법론 소개/ 프로토타입 방법론 소개",
                  time: "4",
                  note: "")
 course5.course_programs.create(
                  lo: "소프트웨어 아키텍처 평가 실습",
                  contents: "실전 사례 소개/ 아키텍처 평가 실습 진행",
                  time: "2",
                  note: "실습 2h")
course6 = Course.create(
                 title: "컴포넌트 설계",
                 purpose: "기능 요구사항을 기준으로 Use Case model을 작성하고 비기능 요구사항을 만족시키는 소프트웨어 아키텍처를 기준으로 Logical Model을 작성할 수 있다. 이를 기준으로 UML 상세설계인 Behavor, Structure, Component Model을 이해하고 만들 수 있다.",
                 feature: "기능 요구사항을 기준으로 Use Case Model을 작성\nUse Case Model과 기 작성된 소프트웨어 아키텍처를 기준으로 상위 설계를 진행\n상위 설계인 Logical model을 기준으로 상세 설계인 Behavior, Structure, Component model을 개발한다",
                 overview_number_of_member: "30",
                 overview_type: "집합교육(비합숙)",
                 overview_target: "중급(소프트웨어 설계/개발 경력 5년차 이상) 소프트웨어 엔지니어 객체 지향 설계 경험이 있거나 앞으로 객체 지향 설계가 필요하신 분",
                 overview_time: "24h(3일)",
                 expectation: "소프트웨어 요구사항 분석의 목적을 이해하고 Use Case 모델을 만들 수 있다. Use Case model을 기준으로 상위 설계를 진행할 수 있고 Logical model을 만들 수 있다. 상위 설계인 Logical model을 기준으로 상세 설계인 Behavior, Structure, Component model을 만들 수 있다.", )
 course6.course_programs.create(
                 lo: "소프트웨어 분석/상위 설계의 개념 이해 및 실습 (LO-1,2)",
                 contents: "소프트웨어 분석의 목적 이해/Use Case model의 이해 및 실습/상위 설계의 목적/4+1 view 아키텍처 이해/Logical Model 이해 및 실습",
                 time: "8",
                 note: "실습 2h")
 course6.course_programs.create(
                 lo: "소프트웨어 상위 설계 진행 및 상세 설계 진행(LO-2,3,4)",
                 contents: "소프트웨어 상세 설계 목적 이해/UML 기반의 소프트웨어 설계 기법 소개/Behavior model 이해 및 실습/Structure model 이해 및 실습",
                 time: "8",
                 note: "실습 2h")
 course6.course_programs.create(
                  lo: "소프트웨어 상세 설게 진행및 평가 (LO-4,5)",
                  contents: "UML 기반의 component model 소개/소프트웨어 Component model의 목적 이해 및 실습 / 소프트웨어 분석/상위/상세 설계 실습 결과 발표",
                  time: "8",
                  note: "실습 2h\n발표 2h")
course7 = Course.create(
                 title: "소프트웨어 테스트",
                 purpose: "소프트웨어 품질을 실질적으로 향상시킬 수 있는 소프트웨어 테스트 기법 및 방법론을 효과적으로 학습하고, 이를 실제 프로젝트에 효과적으로 적용할 수 있도록 실습 위주로 진행한다.",
                 feature: "소프트웨어 테스팅 개념, 생명주기별 테스팅 수준, 블랙박스 시험, 화이트 박스 시험 기법을 배우고 실습한다. SW 테스팅  기본 기술과 모델 기반 시험 방법, 시험 자동화 도구, 시험 프로세스를 포함한 고급 기술을 이해하고 실습한다.",
                 overview_number_of_member: "30",
                 overview_type: "집합교육(비합숙)",
                 overview_target: "중급(소프트웨어 설계/개발 경력 5년차 이상) 소프트웨어 엔지니어\n소프트웨어 품질 보증 담당자\n소프트웨어 고급 개발자\n소프트웨어 개발/시험 관리자",
                 overview_time: "24h(3일)",
                 expectation: "소프트웨어 요구사항 분석의 목적을 이해하고 Use Case 모델을 만들 수 있다. Use Case model을 기준으로 상위 설계를 진행할 수 있고 Logical model을 만들 수 있다. 상위 설계인 Logical model을 기준으로 상세 설계인 Behavior, Structure, Component model을 만들 수 있다.", )
 course7.course_programs.create(
                 lo: "테스트 개념 정립(LO-1)",
                 contents: " 소프트웨어 테스팅 정의 / 개발 생명주기 별 소프트웨어 테스팅 과 생명주기 /  Black-box testing / White-box testing /  Spec. 기반 SW 시험케이스 생성 방법",
                 time: "8",
                 note: "실습 2h")
 course7.course_programs.create(
                 lo: "테스트 기법 소개 및 실습 (LO-2)",
                 contents: "State-Oriented SW 시험 모델 / FSM/EFSM 모델 기반 케이스 생성  방법 / 시험 자동화 도구와 시험결과 분석 방법",
                 time: "8",
                 note: "실습 2h")
 course7.course_programs.create(
                  lo: "테스트 방법론 및 실무 사례 적용(LO-3)",
                  contents: "통합 시험 개념 이해/  시스템 시험 개념 이해/각각의 시험 프로세스 수립/ 가각 시험계획서 작성 시습 및 발표",
                  time: "8",
                  note: "실습 2h\n발표 2h
")

Consulting.all.each { |x| x.delete }

Consulting.create(
  title: '소프트웨어 프로세스 구축',
  banner_txt: '더 효율적인 소프트웨어 개발 프로세스를 통해 경제적인 소프트웨어 개발 환경을 갖춘다면 높은 생산성과 최고의 품질의 소프트웨어를 개발할 수 있습니다',
  desc: 'JU consulting의 프로세스 구축 컨설팅 서비스는 귀사에 적합한 소프트웨어 프로세스를 설계하여 드립니다. 귀사의 비즈니스 도메인과 엔지니어의 능력에 따라 CBD뿐 아니라 최신의 Agile 프로세스를 Customizing하여 귀사에 정착할 수 있도록 도와드립니다. 뿐만 아니라 소프트웨어 품질을 보증할 수 있는 테스트 기법을 제공하고 이에 맞는 도구를 지원하여 드립니다.',
  features: '<ul><li>Agile 프로세스 컨설팅</li><li>Component-Based Development 프로세스 컨설팅</li><li>QA기반의 테스트 지원 컨설팅</li><li>개발 방법론에 적합한 개발 환경 및 도구 지원 컨설팅</li></ul>',
  main_img: 'consulting1_main_img.png',
  etc_img: 'consulting1_etc_img.png')
Consulting.create(
  title: '소프트웨어 분석/설계',
  banner_txt: '귀사의 비즈니스 목표를 만족하는 소프트웨어 시스템을 만들기 위해서는 목적에 맞는 소프트웨어 의 분석/설계를 할 수 있어야 합니다. 효과적이며 정확한 방법으로 소프트웨어를 분석/설계할 수 있는 최신의 분석/설계 방법을 멘토링 해드립니다.',
  desc: 'JU Consulting의 소프트웨어 분석/설계 컨설팅 서비스는 소프트웨어 요구사항 분석에서 아키텍처 설계 기법까지 지원합니다. 또한 아키텍처 설계를 기반으로 프로토타입을 만들어 귀사에 비즈니스 목적을 만족하는 고품질의 소프트웨어를 개발할 수 있도록 지원합니다. 귀사의 소프트웨어 엔지니어는 목적에 맞는 소프트웨어 시스템을 개발할 수 있을 뿐 아니라 최신의 소프트웨어 분석/설계 능력을 갖출 수 있습니다.',
  features: '<ul><li>소프트웨어 요구사항 수집 및 분석 멘토링</li><li>소프트웨어 요구사항에 따른 설계 전략  멘토링</li><li>Technical /Domain Framework 아키텍처 설계 멘토링</li><li>Software Product-Line 가반의  아키텍처 설계 멘토링</li><li>Prototype  방법론을 통한 아키텍처 검증 멘토링</li></ul>',
  main_img: 'consulting2_main_img.png',
  etc_img: 'consulting2_etc_img.png')
Consulting.create(
  title: '소프트웨어 분석/설계 진단',
  banner_txt: '귀사의 소프트웨어 시스템이 바르고 정확하게 설계되었는지 고민하고 계십니까? 새로운 방법과 도구를 이용한 소프트웨어 진단 서비스를 이용하여 소프트웨어 분석/설계에 문제점이 없는지 진단하십시오.',
  desc: 'JU Consulting은 귀사의 소프트웨어 시스템이 비즈니스 목표에 부합하는지 진단하여 드립니다. 뿐만 아니라 소프트웨어 시스템이 반드시 갖춰야 하는 신뢰성, 성능, 확장성, 유지보수성 등 핵심 품질을 확보하고 있는지 진단해 드립니다. CMU-SEI에서 제시하는 최신의 기법을 활용하여 진단함으로 JUConsulting만의 믿을 수 있는 진단 서비스를 귀사에 제공합니다.',
  features: '<ul><li>소프트웨어 요구사항의 완정성 (Completeness) 진단</li><li>소프트웨어 요구사항의 정확정(Correctness) 진단</li><li>소프트웨어 설계 전략의 유효성(Validation)  진단</li><li>소프트웨어 설계 전략의 명확성(Verification) 진단</li><li>소프트웨어 아키텍처 설계 뷰포인트의 적절성 진단</li><li>소프트웨어 아키텍처 설계 평가 및 리스크 진단</li></ul>',
  main_img: 'consulting3_main_img.png',
  etc_img: 'consulting3_etc_img.png')


Consult.all.each { |y| y.delete }

Consult.create(
  title: '소프트웨어 프로세스 구축',
  banner_txt: '더 효율적인 소프트웨어 개발 프로세스를 통해 경제적인 소프트웨어 개발 환경을 갖춘다면 높은 생산성과 최고의 품질의 소프트웨어를 개발할 수 있습니다',
  desc: 'JU consulting의 프로세스 구축 컨설팅 서비스는 귀사에 적합한 소프트웨어 프로세스를 설계하여 드립니다. 귀사의 비즈니스 도메인과 엔지니어의 능력에 따라 CBD뿐 아니라 최신의 Agile 프로세스를 Customizing하여 귀사에 정착할 수 있도록 도와드립니다. 뿐만 아니라 소프트웨어 품질을 보증할 수 있는 테스트 기법을 제공하고 이에 맞는 도구를 지원하여 드립니다.',
  features: '<ul><li>Agile 프로세스 컨설팅</li><li>Component-Based Development 프로세스 컨설팅</li><li>QA기반의 테스트 지원 컨설팅</li><li>개발 방법론에 적합한 개발 환경 및 도구 지원 컨설팅</li></ul>',
  main_img: 'consulting1_main_img.png',
  etc_img: 'consulting1_etc_img.png')
Consult.create(
  title: '소프트웨어 분석/설계',
  banner_txt: '귀사의 비즈니스 목표를 만족하는 소프트웨어 시스템을 만들기 위해서는 목적에 맞는 소프트웨어 의 분석/설계를 할 수 있어야 합니다. 효과적이며 정확한 방법으로 소프트웨어를 분석/설계할 수 있는 최신의 분석/설계 방법을 멘토링 해드립니다.',
  desc: 'JU Consulting의 소프트웨어 분석/설계 컨설팅 서비스는 소프트웨어 요구사항 분석에서 아키텍처 설계 기법까지 지원합니다. 또한 아키텍처 설계를 기반으로 프로토타입을 만들어 귀사에 비즈니스 목적을 만족하는 고품질의 소프트웨어를 개발할 수 있도록 지원합니다. 귀사의 소프트웨어 엔지니어는 목적에 맞는 소프트웨어 시스템을 개발할 수 있을 뿐 아니라 최신의 소프트웨어 분석/설계 능력을 갖출 수 있습니다.',
  features: '<ul><li>소프트웨어 요구사항 수집 및 분석 멘토링</li><li>소프트웨어 요구사항에 따른 설계 전략  멘토링</li><li>Technical /Domain Framework 아키텍처 설계 멘토링</li><li>Software Product-Line 가반의  아키텍처 설계 멘토링</li><li>Prototype  방법론을 통한 아키텍처 검증 멘토링</li></ul>',
  main_img: 'consulting2_main_img.png',
  etc_img: 'consulting2_etc_img.png')
Consult.create(
  title: '소프트웨어 분석/설계 진단',
  banner_txt: '귀사의 소프트웨어 시스템이 바르고 정확하게 설계되었는지 고민하고 계십니까? 새로운 방법과 도구를 이용한 소프트웨어 진단 서비스를 이용하여 소프트웨어 분석/설계에 문제점이 없는지 진단하십시오.',
  desc: 'JU Consulting은 귀사의 소프트웨어 시스템이 비즈니스 목표에 부합하는지 진단하여 드립니다. 뿐만 아니라 소프트웨어 시스템이 반드시 갖춰야 하는 신뢰성, 성능, 확장성, 유지보수성 등 핵심 품질을 확보하고 있는지 진단해 드립니다. CMU-SEI에서 제시하는 최신의 기법을 활용하여 진단함으로 JUConsulting만의 믿을 수 있는 진단 서비스를 귀사에 제공합니다.',
  features: '<ul><li>소프트웨어 요구사항의 완정성 (Completeness) 진단</li><li>소프트웨어 요구사항의 정확정(Correctness) 진단</li><li>소프트웨어 설계 전략의 유효성(Validation)  진단</li><li>소프트웨어 설계 전략의 명확성(Verification) 진단</li><li>소프트웨어 아키텍처 설계 뷰포인트의 적절성 진단</li><li>소프트웨어 아키텍처 설계 평가 및 리스크 진단</li></ul>',
  main_img: 'consulting3_main_img.png',
  etc_img: 'consulting3_etc_img.png')
