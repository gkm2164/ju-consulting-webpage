class CreateConsults < ActiveRecord::Migration
  def change
    create_table :consults do |t|
      t.text :title
      t.text :banner_txt
      t.text :desc
      t.text :features
      t.text :main_img
      t.text :etc_img

      t.timestamps
    end
  end
end
