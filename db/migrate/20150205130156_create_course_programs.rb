class CreateCoursePrograms < ActiveRecord::Migration
  def change
    create_table :course_programs do |t|
      t.string :lo
      t.text :contents
      t.integer :time
      t.string :note
      t.references :course, index: true
      t.timestamps
    end
  end
end
