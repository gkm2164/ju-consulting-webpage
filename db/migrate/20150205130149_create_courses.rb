class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :title
      t.text :purpose
      t.text :feature
      t.integer :overview_number_of_member
      t.string :overview_type
      t.text :overview_target
      t.string :overview_time
      t.text :expectation
      t.timestamps
    end
  end
end
